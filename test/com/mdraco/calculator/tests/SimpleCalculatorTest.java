package com.mdraco.calculator.tests;

import com.mdraco.calculator.data.SimpleCalculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * User: mateusz
 * Date: 16.04.2013
 * Time: 06:47
 * Created with IntelliJ IDEA.
 */
public class SimpleCalculatorTest {
	private SimpleCalculator calculator;

	@Before
	public void setUp() throws Exception {
		this.calculator = new SimpleCalculator();
	}

	@Test
	public void tesAdd()
	{
		Assert.assertEquals("345 + 12 calculated", 345+12, calculator.parseString("345+12").calculate());
	}

	@Test
	public void testMultiply()
	{
		Assert.assertEquals("32 * 67 calculated", 32*67, calculator.parseString("32*67").calculate());
		Assert.assertEquals("32 * 67 == 67 * 32", 32*67, calculator.parseString("67*32").calculate());
	}

	@Test
	public void testWhitespace()
	{
		Assert.assertEquals("whitespace doesn't matter", 12*4, calculator.parseString(" 12 *  4 ").calculate());
	}

	@Test
	public void testMultiplyAndAddition()
	{
		Assert.assertEquals("3 * 3 - 2 calculated", 3 * 3 - 2, calculator.parseString("3 * 3 - 2").calculate());
		Assert.assertEquals("2 - 3 * 3 calculated", 2 - 3 * 3, calculator.parseString("2 - 3 * 3").calculate());
	}

	@Test
	public void testSingleDigit()
	{
		Assert.assertEquals("only number is just a number", 69, calculator.parseString("69").calculate());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testErrorWithOperator()
	{
		calculator.parseString("x*2");
	}

	@Test
	public void testBlock()
	{
		Assert.assertEquals("3 + 3 * 4", 3+3*4, calculator.parseString("3+3*4").calculate());
		Assert.assertEquals("(3 + 3) * 4", (3+3)*4, calculator.parseString("(3+3)*4").calculate());
	}

	@Test
	public void testSubBlocks()
	{
		Assert.assertEquals("(((1+2)*3)+13)/11 = 2", 2, calculator.parseString("(((1+2)*3)+13)/11").calculate());
	}

	@Test
	public void testTooMuchSubstractions()
	{
		Assert.assertEquals("1-2-3 = -4", -4, calculator.parseString("1-2-3").calculate());
	}
}
