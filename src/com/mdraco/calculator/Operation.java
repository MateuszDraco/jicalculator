package com.mdraco.calculator;

/**
 * All available operations and theirs descriptions.
 * User: mateusz
 * Date: 14.04.2013
 * Time: 10:19
 * Created with IntelliJ IDEA.
 */
public abstract class Operation {
	private final Character sign;
	private final int significance;

	/**
	 * Constructor.
	 * @param sign represents operator
	 * @param significance greater significance operators are calculated first
	 */
	private Operation(Character sign, int significance) {
		this.sign = sign;
		this.significance = significance;
	}

	/**
	 * Sign that represents operator.
	 * @return single digit
	 */
	public Character getSign() {
		return sign;
	}

	/**
	 * Significance of an operator.
	 * @return integer value that represents significance
	 */
	public int getSignificance() {
		return significance;
	}

	/**
	 * Calculate two values using current operator.
	 * @param val1 first value
	 * @param val2 second value
	 * @return combined result
	 */
	public abstract int calculate(Integer val1, Integer val2);

	/**
	 * Find valid operator for a character. If not found throws UnsupportedOperationException.
	 * @param c character
	 * @return operation object
	 */
	public static Operation parseChar(Character c) {
		for (Operation operation : AvailableOperations) {
			if (operation.getSign() == c)
				return operation;
		}
		throw new UnsupportedOperationException("Unknown operator: " + c);
	}

	/**
	 * Summary operation object.
	 */
	public static Operation Add = new Operation('+', 5) {
		/**
		 * Does a addition.
		 * @param val1 first value
		 * @param val2 second value
		 * @return summary of two values
		 */
		@Override
		public int calculate(Integer val1, Integer val2) {
			return val1 + val2;
		}
	};
	/**
	 * Subtract operation.
	 */
	public static Operation Subtract = new Operation('-', 10) {
		/**
		 * Does a subtract.
		 * @param val1 first value
		 * @param val2 second value
		 * @return val2 minus val1
		 */
		@Override
		public int calculate(Integer val1, Integer val2) {
			return val1 - val2;
		}
	};
	/**
	 * Multiply operation.
	 */
	public static Operation Multiply = new Operation('*', 20) {
		/**
		 * Does a multiplication.
		 * @param val1 first value
		 * @param val2 second value
		 * @return val1 over val2
		 */
		@Override
		public int calculate(Integer val1, Integer val2) {
			return val1 * val2;
		}
	};
	public static Operation Divide = new Operation('/', 20){
		@Override
		public int calculate(Integer val1, Integer val2) {
			return val1 / val2;
		}
	};
	public static Operation BlockBegin = new Operation('(', 50){
		@Override
		public int calculate(Integer val1, Integer val2) {
			throw new UnsupportedOperationException("Blocks should be calculated otherwise.");
		}
	};
	public static Operation BlockEnd = new Operation(')', 50){
		@Override
		public int calculate(Integer val1, Integer val2) {
			throw new UnsupportedOperationException("Blocks should be calculated otherwise.");
		}
	};

	public static Operation[] AvailableOperations = new Operation[] {
			Add,
			Subtract,
			Multiply,
			Divide,
			BlockBegin,
			BlockEnd
	};
}
