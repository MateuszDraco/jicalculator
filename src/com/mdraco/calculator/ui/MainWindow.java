package com.mdraco.calculator.ui;

import com.mdraco.calculator.Operation;
import com.mdraco.calculator.data.SimpleCalculator;

import javax.swing.*;
import java.awt.*;

/**
 * Application's main window
 * User: mateusz
 * Date: 13.04.2013
 * Time: 14:52
 * Created with IntelliJ IDEA.
 */
public class MainWindow extends JFrame {
	private final ResultsPanel results;

	public MainWindow() {
		super("Calculator");

		this.results = new ResultsPanel();
		DigitsPanel digits = new DigitsPanel() {
			@Override
			public void performDigit(int digit) {
				results.addInput(Integer.toString(digit));
			}
		};
		OperationsPanel operations = new OperationsPanel() {
			@Override
			protected void performResultExecution() {
				try {
					int result = new SimpleCalculator(results.getInput()).calculate();
					results.setOutput(Integer.toString(result));
				} catch (UnsupportedOperationException exception) {
					results.setOutput(exception.getMessage());
				}
			}

			@Override
			protected void performClear() {
				results.setInput("");
			}

			@Override
			protected void performOperation(Operation operation) {
				results.addInput(operation.getSign().toString());
			}
		};

		setSize(400, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		setLayout(new BorderLayout(20, 10));
		add(results, BorderLayout.NORTH);
		add(digits, BorderLayout.CENTER);
		add(operations, BorderLayout.EAST);
	}
}
