package com.mdraco.calculator.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel with digit buttons.
 * User: mateusz
 * Date: 14.04.2013
 * Time: 09:47
 * Created with IntelliJ IDEA.
 */
public abstract class DigitsPanel extends JPanel implements ActionListener {
	/**
	 * Constructor
	 */
	public DigitsPanel() {
		setLayout(new GridLayout(4, 3));

		for (int i = 1; i < 10; ++i)
			add(new DigitButton(this, i));
		add(new JLabel(""));
		add(new DigitButton(this, 0));
	}

	/**
	 * Internal class for button with specific digit.
	 */
	private class DigitButton extends JButton
	{
		private final int digit;

		/**
		 * Constructor.
		 * @param actionListener listener for click
		 * @param digit number that will be displa
		 */
		public DigitButton(ActionListener actionListener, int digit)
		{
			super(Integer.toString(digit));

			this.digit = digit;
			this.addActionListener(actionListener);
		}

		/**
		 * returns a number under button
		 * @return number that is stored in a button
		 */
		public int getDigit() {
			return digit;
		}
	}

	/**
	 * Button clicks.
	 * @param actionEvent event's data
	 */
	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() instanceof DigitButton) {
			DigitButton button = (DigitButton)actionEvent.getSource();
			performDigit(button.getDigit());
		}
	}

	public abstract void performDigit(int digit);
}
