package com.mdraco.calculator.ui;

import com.mdraco.calculator.Operation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel with operations.
 * User: mateusz
 * Date: 14.04.2013
 * Time: 10:02
 * Created with IntelliJ IDEA.
 */
public abstract class OperationsPanel extends JPanel implements ActionListener {
	/**
	 * Constructor
	 */
	public OperationsPanel() {
		int rows = (int) Math.ceil(Operation.AvailableOperations.length / 2.0);
		setLayout(new GridLayout(rows + 1, 2));
		for (Operation operation : Operation.AvailableOperations) {
			add(new OperationButton(this, operation));
		}

		add(new ResultButton(this));
		add(new ClearButton(this));
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() instanceof OperationButton) {
			OperationButton operation = (OperationButton)actionEvent.getSource();
			performOperation(operation.getOperation());
		}
		else if (actionEvent.getSource() instanceof ResultButton) {
			performResultExecution();
		}
		else if (actionEvent.getSource() instanceof ClearButton) {
			performClear();
		}
	}

	protected abstract void performResultExecution();

	protected abstract void performClear();

	protected abstract void performOperation(Operation operation);
	private class OperationButton extends JButton {
		public Operation getOperation() {
			return operation;
		}

		private final Operation operation;

		public OperationButton(ActionListener listener, Operation operation) {
			super(operation.getSign().toString());

			this.operation = operation;
			this.addActionListener(listener);
		}
	}
	private class ResultButton extends JButton {
		private ResultButton(ActionListener listener) {
			super("=");

			addActionListener(listener);
		}
	}
	private  class ClearButton extends JButton {
		private ClearButton(ActionListener listener) {
			super("C");

			addActionListener(listener);
		}
	}
}
