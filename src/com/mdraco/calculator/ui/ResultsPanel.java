package com.mdraco.calculator.ui;

import sun.awt.HorizBagLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: mateusz
 * Date: 13.04.2013
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public class ResultsPanel extends JPanel {
	private final JTextField input;
	private final JTextField output;

	public ResultsPanel() {
		input = new JTextField();
		input.setMaximumSize(new Dimension(600, 20));
		input.setMinimumSize(new Dimension(100, 20));
		input.setEditable(false);

		output = new JTextField();
		output.setMaximumSize(new Dimension(200, 20));
		output.setMinimumSize(new Dimension(50, 20));
		output.setEditable(false);

		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		add(input);
		add(new JLabel("="));
		add(output);
	}

	public String getInput()
	{
		return input.getText();
	}

	public void setOutput(String text)
	{
		output.setText(text);
	}

	public void setInput(String text)
	{
		input.setText(text);
	}

	public void addInput(String text)
	{
		input.setText(input.getText() + text);
	}
}
